import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';  
import { AppRoutingModule } from './routes/app-routing.module';
import { AppComponent } from './app.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { GifModule } from './gif/gif.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    GifModule
  ],
  providers: [
    FormBuilder
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
