import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from '../app.component';
import { UploadComponent } from '../gif/upload/upload.component';

const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'gif-upload', component: UploadComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
