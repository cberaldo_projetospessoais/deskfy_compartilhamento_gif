
export interface IGif {
  arquivo: String;
  visibilidade: Number;
  senha: String;
  data_expiracao: Date
}

export class Gif {
  arquivo: String;
  visibilidade: Number;
  senha: String;
  data_expiracao: Date;
}

export class GifList extends Array<Gif> {
}