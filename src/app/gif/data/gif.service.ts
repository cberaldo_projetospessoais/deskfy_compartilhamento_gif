import { GifList, Gif } from './gif.model';

export class GifService {
    items : GifList = [];

    add(gif : Gif) {
      this.items.push(gif);
    }

    getGifs() : GifList {
      return this.items;
    }

    getGif() : Gif {
      return this.items[0];
    }
  }