import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { GifService } from '../data/gif.service';
import { GifList } from '../data/gif.model';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.styl']
})
export class UploadComponent implements OnInit {
  gifForm : FormGroup;
  gifs : GifList = []

  enviarGif() {
    const { arquivo, data_expiracao, senha, visibilidade } = this.gifForm.controls;

    const gif = {
      arquivo: arquivo.value,
      visibilidade: visibilidade.value,
      senha: senha.value,
      data_expiracao: data_expiracao.value
    };

    this.gifService.add(gif);
    this.gifs = this.gifService.getGifs();
    this.gifForm.reset();
  }

  constructor(
    private gifService: GifService,
    private formBuilder: FormBuilder
  ) {
    this.gifForm = this.formBuilder.group({
      arquivo: ['', Validators.required],
      visibilidade: ['', Validators.required],
      data_expiracao: ['', Validators.required],
      senha: ['']
    });
  }

  ngOnInit() { }
}
